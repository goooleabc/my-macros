/* 
name: separate_lines.js
*/
/*
var item ;
while ( item = prompt("请输入您将要分割的对象代码：1:作者；2:单位；3:作者和单位","1") ) {
    if (item == 3 || item == 1 || item == 2) { break; }
}

var my_item = new Array(
            // 0 element refers to nothing
            ""
            // 1 refers to author column
            //  6	尿酸钠致急性痛风性关节炎模型大鼠与槲皮素的抗炎作用 	 黄敬群; 孙文娟;王四旺; 朱妙章;李舒烨	 解放军第四军医大学药学系天然药物学教研室; 吉	
            ,new Array("作者",      "\^((?:[\^\\t]*?\\t){2})\\s*([^\\t]*?);\\s*(.*?)\\t(.*)"     ,"$1$2\\t$4\\n$1$3\\t$4")
            // 2 refers to author's unit column
            ,new Array("单位",      "\^((?:[\^\\t]*?\\t){3})\\s*([^\\t]*?);\\s*(.*?)\\t(.*)"     ,"$1$2\\t$4\\n$1$3\\t$4")
            // 3 refers to author column and author's unit column
            ,new Array("作者和单位","\^((?:[\^\\t]*?\\t){2,3})\\s*([^\\t]*?);\\s*(.*?)\\t(.*)"    ,"$1$2\\t$4\\n$1$3\\t$4")
        );

var str_find    = my_item[item][1];
var str_replace = my_item[item][2];
Replace_do(str_find,str_replace);
*/
/* 以上程序代码的优化方案,如下 */
/*
LOG:
    2014.04.30 xyw 添加了运行中结束程序的代码 
*/
myBlock: {
    var column = "";
    var separator;
    
    while ( column == "") {
        column = prompt("您要分割的文字位于第几列？首列为第1列，本脚本不能分割首尾列。","");
        if ( column.search(/^\d+$|^\d+,\d+$/) > -1  ) {   break;}  // 匹配成功,则退出
        else if ( column.search(/q|x/) > -1 ) { break myBlock; }
        else { alert("请输入阿拉伯数字。如：1,2,3等；“2,4”代表分割第2到4列");}
        column = "";     // 重定义column
    }
    column = column - 1;
    while ( separator = prompt("请输入分割符,如“;,”等，“[;；]”代表2种分号分隔符。正则符号请转义。",";") ) {
        if ( separator.search(/q|x/) > -1 ) { break myBlock; }
        if ( confirm("您确认输入的分隔符【"+separator+"】，否则，取消后重新输入。") ) { break;}
        // else { throw new Error("quit!"); }
    }
    if (separator != '') {    
        var str_find    = "\^((?:[\^\\t]*?\\t){"+column+"})\ *([^\\t]+?)\ *"+separator+"\ *(.*?)\\t(.*)";
        var str_replace = "$1$2\\t$4\\n$1$3\\t$4";
        Replace_do(str_find,str_replace);
    }
}
function Replace_do(str_find,str_replace) {
    document.selection.StartOfDocument(false);
    while 
    (document.selection.Find( str_find ,eeFindNext | eeFindSaveHistory | eeFindReplaceEscSeq | eeFindReplaceRegExp)) {
        document.selection.Replace( str_find ,str_replace ,eeFindNext | eeFindSaveHistory | eeFindReplaceEscSeq | eeFindReplaceRegExp);
        document.selection.StartOfDocument(false);
    }
}

