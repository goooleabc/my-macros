﻿/*
name:   format_refs_for_liu.js
Original text:
序号	 题名	 作者	 作者单位	 文献来源	 发表时间	 被引频次	 下载频次
 1	被美国FDA和欧洲EMA认可的肾脏生物标志物在药物安全性评价与预测中的意义 	 谢红光; 王书奎; 曹长春; Ernie Harpur	 美国食品与药品管理局（FDA）临床药理学办公室（CDER/OTS/OCP）; 南京医科大学南京第一医院; 英国Newcastle大学医学院	【中国会议】第十三次全国临床药理学学术大会论文汇编	 2012-10-26	 	  14

Needed format:
被美国FDA和欧洲EMA认可的肾脏生物标志物在药物安全性评价与预测中的意义
	文献信息
		作者
			谢红光
			王书奎
			曹长春
			Ernie Harpur
		单位
			美国食品与药品管理局（FDA）临床药理学办公室（CDER/OTS/OCP）
			南京医科大学南京第一医院
			英国Newcastle大学医学院
		来源
			【中国会议】第十三次全国临床药理学学术大会论文汇编
		发表时间
			2012-10-26
LOG：
    This script was created for Mr. Liu to change cnki reference results as mind road's format.
        optimized by xyw，2013.08.13.
        This script was only tested on EmEditor Professional (32-bit) version 11.0.3 on windows platform.
    0.2 optimized by xyw，2013.09.30.
        fix a bug to deal with NUMber id more than 10.
    
*/



    var str_find    = "\^ *?\\d+\\t(.+?)\\t(.+?)\\t(.+?)\\t(.+?)\\t(.+?)\\t(.*)";
    var str_replace = "'$0\n\n$1\n\t文献信息\n\t\t作者\n\t\t\t$2\n\t\t单位\n\t\t\t$3\n\t\t来源\n\t\t\t$4\n\t\t发表时间\n\t\t\t$5";
    Replace_do(str_find,str_replace);
	
    str_find    = "\^\\t\\t\\t(.+?);(.+?)\$";
    str_replace = "\\t\\t\\t$1\\n\\t\\t\\t\\2";
    Replace_do(str_find,str_replace);
    
function Replace_do(str_find,str_replace) {
    document.selection.StartOfDocument(false);
    while 
    (document.selection.Find( str_find ,eeFindNext | eeFindSaveHistory | eeFindReplaceEscSeq | eeFindReplaceRegExp)) {
        document.selection.Replace( str_find ,str_replace ,eeFindNext | eeFindSaveHistory | eeFindReplaceEscSeq | eeFindReplaceRegExp);
        document.selection.StartOfDocument(false);
    }
}

